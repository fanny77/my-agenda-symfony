<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request as Request;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact/{id}", name="contact",  requirements= {"id"="\d+"})
     */
    public function contact(ManagerRegistry $doctrine, $id): Response
    {

        $repository = $doctrine->getRepository(Contact::class);
        $contact = $repository->find($id);

        if (!$contact) {
            throw $this->createNotFoundException('Aucun contact n\'a été trouvé pour ' .$id);
        }
        
        return $this->render('home/contact.html.twig', [
            'contact' => $contact
        ]);
    }

    /**
     * @Route("/contact/add", name="AddContact")
     */
    public function create(ManagerRegistry $doctrine, Request $request): Response
    {
        $contact = new Contact();
        
        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();

            $entityManager = $doctrine->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Contact ajouté avec succès'
            );
            
            return $this->redirectToRoute('home');
        }

        return $this->render('home/ajouter.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/contact/delete/{id}", name="DeleteContact", requirements={"id"="\d+"})
     */
    public function delete(ManagerRegistry $doctrine,$id): Response
    {
        $entityManger = $doctrine->getManager();
        $repository = $doctrine->getRepository(Contact::class);

        $contact = $repository->find($id);
        if (!$contact) {
            throw $this->createNotFoundException('Aucun contact n\'a été trouvé pour ' .$id);
        }
        $entityManger->remove($contact);
        $entityManger->flush();

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/contact/update/{id}", name="UpdateContact", requirements={"id"="\d+"})
     */
    public function update(ManagerRegistry $doctrine,$id, Request $request): Response
    {
        $repository = $doctrine->getRepository(Contact::class);
        $contact = $repository->find($id);
        if (!$contact) {
            throw $this->createNotFoundException('Aucun contact n\'a été trouvé pour ' .$id);
        }
        
        $form = $this->createForm(ContactType::class, $contact);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $entityManger = $doctrine->getManager();

            $entityManger->flush();
            
            $this->addFlash(
                'notice',
                'Contact modifié avec succès'
            );
            
            return $this->redirectToRoute('home');
        }

        return $this->render('home/modifier.html.twig',[
            'form' => $form->createView()
        ]);
    }
}