<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Contact;
use App\Repository\ContactRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ManagerRegistry $doctrine): Response
    {
        $age = 18;
       
        /**
         * @var ContactRepository
         */
        $contactRepo = $doctrine->getRepository(Contact::class);
        $contacts = $contactRepo->findAllGreaterThanAge($age);

        return $this->render('home/home.html.twig',[
            'contacts' => $contacts
        ]);
    }

    /**
     * @Route("/contacts/list", name="contactList")
     */
    public function contactList(ManagerRegistry $doctrine): Response
    {
        $contacts = $doctrine->getRepository(Contact::class)->findAll();

        return $this->render('home/home.html.twig',[
            'contacts' => $contacts,
        ]);
    }
}
